# -*- coding: utf-8 -*-
"""
Created on Fri Apr  3 10:19:48 2020

@author: Léa

On souhaite présenter la progression des élèves au cours de la formation Data
du DigiFab du Havre dans une application web écrite à l'aide du framework
Flask et d'une base de données SQLite3. La représentation graphique des
connaissances acquises au fil de la formation sera réalisée à l'aide d'une
librairie interne au projet nommée "graphs" utilisant matplotlib et networkx.  

Sous Windows, on ne peux pas utilisé correctement à travers spyder les fonction
debug de flask, il faut donc mettre .

Il faut que le pare-feu ouvre les ports 5000 UDP/TCP

s'assurer que les cookie n'entrave pas la connexion 
(Ctrl+F5 ou efface les cookies)

"""

# Librairies internes au projet 
from bdd import initialisation_base_de_donnees, acquisition_donnees
from graphs import graph_analyse_global, arbre_connaissance, histogramme_notes
from graphs import graphique_progression_globale, graphique_progression
from graphs import dessine_arbre_competences, graph_grappes_hexagones

# Librairies externes
from flask import (Flask, render_template, request, redirect, session)
import sqlite3 as lite
import pandas as pd
from datetime import date, datetime
import time 
from IPython.display import HTML
from werkzeug.security import generate_password_hash, check_password_hash
import os.path

from test_env import arbre_de_la_promo, arbre_personnel,noeud_connaissance
### Déclaration des constantes
ADRESSE_BASE_DE_DONNEES = "base_fictive.db"
ADRESSE_BDD_AUTH = "test116.db"
FICHIER_INITIALISATION = "initialisation_base.xlsx"

# Liste dans les sélections
RANG = [1,2,3]
NOTE = [10,9,8,7,6,5,4,3,2,1,0]
MODE = ['Ajouter','Modifier','Suprimer']
DATE_DEFAUT = date(2020,6,1) 


# Initialisation de l'application web avec Flask
app = Flask(__name__)
app.secret_key = b'{\r\xd9\xe8\x98\xe3<\x15,=\x1et\xd2\xf7\xa5n'



# Pour écrire les programmes dans un premier temps et tester le fonctionnement
# du site web, on génère des données aléatoires et une base de données
# statique. Il faudra faire évoluer cela en se connectant directement à la 
# base de données réelle contenant les véritables notes renseignées au fil du
# temps par les formateurs. 
donnees = acquisition_donnees(ADRESSE_BASE_DE_DONNEES)

if not os.path.isfile(ADRESSE_BASE_DE_DONNEES):
    initialisation_base_de_donnees(mode='rechargement', fichier=FICHIER_INITIALISATION)

@app.route('/')   #CLEAN
def page_accueil():
    """ Page d'accueil du site Web, avec le logo de DigiFab et les boutons  
    permettant d'accéder aux pages de visualisation des connaissances de la 
    promo et des élèves. La page donne aussi une explication du site et de sa
    philosophie. Regardez dans templates/index.html pour accéder plus 
    précisément au contenu de la page.
    """
    return render_template("index.html")

@app.route("/eleve_eval_select") 
def eleve_eval_select():
    """ page selection eleve ou eval pour table evaluation
    """
    # défini la date par default dans le menu déroulant du calendrier
    #PS : Le tri par période n'a pas été implémenté (nous n'avons pas réussi)
    # Mais pour le groupe suivant, nous avons conservé le HTLM avec le
    # calendrier (qui ne renvoit à rien pour le moment, peu importe la date choisie)
    # et nous avons garder dans la fonction suivante le code utilisé que nous
    # avons annoté car sinon il empêche le code de base de fonctionner.
    
    con=lite.connect(ADRESSE_BASE_DE_DONNEES)#connexion à la base
    with con:
        # création de deux variable
        df=pd.read_sql_query( 'SELECT DISTINCT nom_ FROM personnes', con)
        df2=pd.read_sql_query( 'SELECT DISTINCT evaluateur FROM evaluations', con)
        eleve = df["eleve"].tolist()
        formateur = df2["evaluateur"].tolist()
        # Les éléments des colonnes eleve et evaluateur sont transformé en liste
    
        
    return render_template('eleve_eval_select.html',
                           membres= eleve,
                           formateurs = formateur,
                           maintenant = DATE_DEFAUT)

@app.route('/table_evaluation', methods=['POST']) #A modifier
def print_table_evaluation():
    """ Page contenant la table des évaluations de la base de données SQLite,
    avec l'ensemble des notes des élèves.
    """
    # avec les request.form, on appelle les choix fait dans le html.
    eleve = request.form['eleve']
    formateur = request.form['formateur']
    date_debut = datetime.strptime(request.form['date_debut'],"%Y-%m-%d").date()
    date_fin = datetime.strptime(request.form['date_fin'],"%Y-%m-%d").date()
                                   
    requetedate=f" and date > '{date_debut}' and date < '{date_fin}'"
    
    # Accès aux données via pandas et la base de données
    con=lite.connect(ADRESSE_BASE_DE_DONNEES)

    with con:
        
        # les if sont la pour executer la demande suivant les choix fait
        # à la page précédente
        if eleve=="none" and formateur != "none":
            df = pd.read_sql_query (f'''select * from evaluations where evaluateur
                                    = "{formateur}"'''+requetedate, con)
        
        elif eleve !="none" and formateur=="none":
            df = pd.read_sql_query (f'''select * from evaluations where eleve 
                                    = "{eleve}"'''+requetedate, con)
        
        elif eleve != "none" and formateur != "none":
            df = pd.read_sql_query (f'''select * from evaluations where eleve 
                                    = "{eleve}" and evaluateur = "{formateur}"'''+requetedate, con)
            
        elif eleve=="none" and formateur=="none":
            df = pd.read_sql_query('select * from evaluations', con)
       # une variable df contenant les lignes selectionnées suivant le choix
            # prédédent seront utilisées dans le code çi dessous pour créer une
            # table affichée directement sur la page
    
    # Tranformation de la dataframe en code html
    table_html = HTML(df.to_html(classes='table table-bordered table-hover'))
    colonnes = df.columns.values
    
    # Ecriture des données dans le template tables.html
    return render_template('table_evaluation.html',
                           table=table_html,
                           titles=colonnes)

@app.route('/form_eval_promo')
def formulaire_eval_promo():
    '''sur cette page l'utilisateur devra remplir un formulaire de type 
       date et methode post qui lui permetera de choisir la date vers laquelle
       toutes les notes de la promo seront affiché pour un "rang" de competence
       precise obtenu a partir de la constante RANG adapté à base de données.
       la date par défaut est obtenu grace à datetime
    '''
    return render_template("formulaire_graph_global.html",
                           liste_de_rang = RANG,
                           maintenant = DATE_DEFAUT, 
                           retourType=["Analyse", "Progression"])

@app.route('/graph_analyse', methods=['POST'])
def evaluation_promo():
    '''cette page doit être atteinte avec un formulaire post lié à la requête
       depuis /form_eval_promo, nous sortons les informations du post, nous 
       les modifions dans des format utilisable car ces informations sont
       transmise sous forme de string puis nous utilisons les méthodes graph
       pour cree l'image et formons la page de resultat avec les informations
       voulu'
    '''
    #les paramètre d'une méthode post sont en string donc nous convertisson 
    rang = int(request.form['rang'])
    print(rang, request.form)
    retourType = request.form['graph']
    print(retourType)
    #nous utilisons strptime avec un regex pour obtenir un objet datetime
    date_observation = datetime.strptime(request.form['date_formulaire'],
                                         "%Y-%m-%d")
    #nous pointons vers l'objet date crée a partir de l'objet datetime obtenu
    date_observation = date_observation.date() 

    if retourType == "Analyse":
        img = "imgs/analyse_promo.png"
        graph_analyse_global(rang, date_observation, "static/"+img, donnees)

    elif retourType == "Progression":
        date_debut, date_fin = date(2020, 3, 1), date(2020, 10, 1)
        img = "imgs/progression_promo.png"
        graphique_progression_globale(date_debut, date_fin, donnees, "static/"+img)
        
    return render_template("graph_analyse.html",
                           rang=rang,
                           date_observation=date_observation,
                           img_analyse=img,
                           time=int(time.time()))

@app.route('/formulaire_modification')
def ajout_evaluations():
    """ Génération de la page permettant d'ajouter de nouvelles évaluations au
    fil du temps pour chacun des élèves. 
    
    Ici il faudra faire un formulaire permettant a l'utilisateur de modifier
    une note d'un élève précis à une date precise 
    
    la page HTML devra avoir une liste d'eleve, une liste d'evaluateur 
    une liste matiere un calendrier
    et renvoyer le formulaire remplis vers /traitement_saisie_modification
    pour y etre transfomer en requete SQL    
    """
    
    #Nous nous connectons a la base de données pour obtenir les info
    con=lite.connect(ADRESSE_BASE_DE_DONNEES)
    with con:
        #une liste d'eleve
        df=pd.read_sql_query( 'SELECT DISTINCT eleve FROM evaluations', con)
        #Transformons la dataframe en list pour le HTML
        eleve = df["eleve"].tolist()
        #même chose pour une liste de connaissance
        df=pd.read_sql_query('SELECT DISTINCT connaissance FROM evaluations', con)
        connaissance = df["connaissance"].tolist()
        #même chose pour une liste d'evaluateur  
        df=pd.read_sql_query('SELECT DISTINCT evaluateur FROM evaluations', con)
        evaluateur = df["evaluateur"].tolist()
        
    
    #Nous envoyons toutes les donnee necessaire au formulaire avec la template
    return render_template("formulaire_modification.html",
                           liste_eleve = eleve,
                           liste_connaissance = connaissance,
                           liste_evaluateur = evaluateur,
                           liste_note = NOTE,
                           liste_mode = MODE,
                           maintenant = datetime.now().date())

@app.route('/traitement_saisie_modification', methods=['POST']) #QuasiFini
def traitement_modification():
    """Nous sommes donc revenu du formulaire modification ici nous traitons les
       donées ainsi obtenu, par clarté nous extrayons les données du formulaire
       dans des variable locale a la fonction, cette étape peut être sauter
       pour economiser de la mémoire vive du serveur
       
       après nous nous connectons à la base de donéee selont le mode,
       en theorie une fonction d'access et de modification de la BDD devrais 
       etre importé dans le serveur mais dans cette version du code les requete
       sont stocké en format string dans le serveur quand elle sont necessaire
       
       il est a noté que dans cette version ce code ne gère aucune erreur
       l'utilisateur peut modifier ou suprimer une donnée non existante et 
       aucune erreur ne lui sera notifier, une modification reussie redirige
       vers la page de modification, encore sans notification de réussite"""
    
    con =lite.connect(ADRESSE_BASE_DE_DONNEES)
    df = pd.read_sql("SELECT prenom, id_eleve FROM 'eleves'", con)
    df.set_index("prenom", inplace=True)
    eleves = df.to_dict()["id_eleve"]

    id_eleve = eleves[request.form['nom_eleve']]
    note = int(request.form['note'])
    connaissance = request.form['connaissance']
    nom_eleve = request.form['nom_eleve']
    date_formulaire = request.form['date_formulaire']
    evaluateur = request.form['evaluateur']
    mode = request.form['mode']
    
    con=lite.connect(ADRESSE_BASE_DE_DONNEES)
    with con:
        cur = con.cursor()
        if mode == "Ajouter" : 
            requete_SQL = f"""INSERT INTO evaluations VALUES('{id_eleve}',
                                                             '{note}',
                                                             '{connaissance}',
                                                             '{nom_eleve}',
                                                             '{date_formulaire}',
                                                             '{evaluateur}')"""
            cur.execute(requete_SQL)
            cur.close()
            
        elif mode == "Modifier" :
            requete_SQL = f"""UPDATE evaluations SET note ='{note}'
                                     WHERE connaissance   ='{connaissance}'
                                     AND   id             ='{id_eleve}'
                                     AND   date           ='{date_formulaire}'
                                     AND   evaluateur     ='{evaluateur}'"""
            cur.execute(requete_SQL)
            cur.close() 
                        
        elif mode == "Suprimer" :
            requete_SQL = f"""DELETE FROM evaluations
                                     WHERE connaissance   ='{connaissance}'
                                     AND   id             ='{id_eleve}'
                                     AND   date           ='{date_formulaire}'
                                     AND   evaluateur     ='{evaluateur}'
                                     AND   note           ='{note}'"""
            cur.execute(requete_SQL)
            cur.close() 
                          
    return redirect("/formulaire_modification")   

@app.route('/profil_select') 
def profil_select():
    print(f"voici la date par defaut {DATE_DEFAUT}")
    con=lite.connect(ADRESSE_BASE_DE_DONNEES)# connexion à la base de donnée
    with con:
        df=pd.read_sql_query( 'SELECT DISTINCT eleve FROM evaluations', con)
        #importation de la table evaluation au sein de la variable df
        eleve = df["eleve"].tolist()
        #les éléments de la colonne eleve sont transformé en liste.

    types_graph = ['Histogramme','Arbre', 'Hexagones', 'Progression']    
    return render_template("profil_select.html",
                           liste_de_rang = RANG,
                           maintenant = DATE_DEFAUT,
                           membres=eleve,
                           retourType=types_graph)

@app.route('/profil', methods=['POST'])
def profil():
    # les request.form servent à retenir les choix fait dans le .HTML précédent
    rang = int(request.form['rang'])
    date_observation = datetime.strptime(request.form['date_formulaire'],
                                         "%Y-%m-%d")
    # la date du formulaire est repassée en AAAA-MM-JJ
    date_observation = date_observation.date()
    eleve = request.form['eleve']
    type_graphique = request.form['type_graphique']
    title = "Profil de " + eleve
    image =""
    
    # les if déterminent le type de graphique souhaité à la page précédente.
    # le code utilisé n'étant pas le même suivant le type de graphe choisi
    if type_graphique == "Histogramme":
        G = arbre_connaissance(eleve, date_observation, donnees)
        image = "imgs/histogramme_"+ str(eleve)+'.png'
        histogramme_notes(G, "static/"+image)
        
    elif type_graphique == "Arbre":
        G = arbre_connaissance(eleve, date_observation, donnees)
        image = "imgs/arbre_"+str(eleve)+'.png'
        dessine_arbre_competences(G, eleve, date_observation, "static/"+image)

    elif type_graphique == "Hexagones":
        G = arbre_connaissance(eleve, date_observation, donnees)
        image = "imgs/hexagones_"+str(eleve)+'.png'
        graph_grappes_hexagones(G, "static/"+image, rang)
        
    elif type_graphique == "Progression":
        date_debut = date(2020, 3, 1)
        date_fin = date(2020, 10, 1)
        image = "imgs/progression_"+str(eleve)+'.png'
        graphique_progression(eleve, date_debut, date_fin, donnees,"static/"+image)
        
        
    return render_template("profil.html",
                           rang=rang,
                           date_observation=date_observation, 
                           eleve=eleve,
                           type_graphique=type_graphique, 
                           title=title,
                           image=image,
                           time=int(time.time()))


@app.route('/creation_compte')
def creation_compte():
    """Ici nous envoyons une template qui permet la creation d'un compte.
       les informations nécéssaire sont les suivante : un nom de compte
       une addresse mail, et le mot de passe désiré en double"""
    if 'nom_de_compte' in session :
        return redirect("/")   
    return render_template("formulaire_creation_compte.html")


@app.route('/traitement_creation_compte', methods=['POST'])
def traitement_creation_compte() :
    """Nous traiton le contenu du formulaire obtenu de la page de création
       Il est a noté que par sécurité le mot de passe de l'utilisateur ne doit
       JAMAIS être stocké sur la BDD en clair, et de limité au maximum les
       sotckage en variable en clair, si la requête est fait en HTTPS le mdp
       sera stocké au sein du formulaire encodé et donc ne sera jamais present
       sur le site sous quelque forme que ce soit en clair"""
    
    #Recuperation données formulaire
    nom_de_compte = request.form['nom_de_compte']
    mail = request.form['mail']
    niveau_de_compte = int(request.form['niveau_de_compte'])
    
    #Verification que les mdp sont les même
    if request.form['mot_de_passe_1'] != request.form['mot_de_passe_2'] :
        return redirect("erreur_mdp")                           
    
    #Debut des operation
    con=lite.connect(ADRESSE_BDD_AUTH)
    with con:
        #Obtenir l'id par raport au dernier utilisateur
        df = pd.read_sql_query(f"""SELECT * FROM authentification 
                                   ORDER BY id_auth DESC LIMIT 1""",con)
        id_auth = df['id_auth'][0] + 1 
        
        #Verifion si le compte existe deja pour cela on récupère les resultat
        #D'une querry pour le nom du compte dans une dataframe panda
        df = pd.read_sql_query(f"""SELECT DISTINCT nom_de_compte
                                   FROM authentification
                                   WHERE nom_de_compte = '{nom_de_compte}'"""
                                                                        , con)
        #si la querry a donné 0 resultat le compte n'existe donc pas
        if df.empty:
            #Si non sallage du mot de passe avec werkzeug security
            mot_de_passe_salé = generate_password_hash(
                                                request.form['mot_de_passe_1'])
            cur = con.cursor()
            requete_SQL = f"""
                INSERT INTO authentification VALUES('{id_auth}',
                                                    '{niveau_de_compte}',
                                                    '{nom_de_compte}',
                                                    '{mail}',
                                                    '{mot_de_passe_salé}')"""
            cur.execute(requete_SQL)
            cur.close()
            #Comunique a l'utilisateur le succès 
            return redirect('/creation_success')
        #Si le compte existe deja redirige vers une page qui indique l'echec
        #de sa tentative de creation de compte
        return redirect('/creation_fail')
    

@app.route('/changer_mdp')
def changer_mdp() :
    '''Formulaire de changement de mot de passe (mdp), demande le nom de compte,
    l'ancien mdp et le nouveau mdp. '''
    return render_template("formulaire_mdp.html")


@app.route('/traitment_mdp', methods=['POST'])
def traitement_mdp():
    '''Si le nom de compte existe, que l'ancien mdp est bon update la bdd avec 
    le nouveau mdp redirige vers une page qui dit vous avez reussi'''
    
    mail = request.form['mail']
    
    #Gestion de l'erreur de faute du double mdp
    if request.form['mot_de_passe_1'] != request.form['mot_de_passe_2'] :
        return redirect("/erreur_mdp")
    
    con=lite.connect(ADRESSE_BDD_AUTH)
    with con:
        #Verifie si le compte existe deja
        df=pd.read_sql_query(f'''SELECT DISTINCT mail FROM authentification
                                 WHERE mail = "{mail}" ''', con)
                                 
        #Verifie si le compte correspond au mot de passe                        
        if not df.empty:
            #Obtention du mdp salé
            df=pd.read_sql_query(f'''SELECT DISTINCT mot_de_passe 
                                     FROM authentification
                                     WHERE mail = "{mail}"''', con)
            #Il est important de noté que l'ont doit comparé un mdp salé
            #à un mdp non salé                         
            if check_password_hash(df['mot_de_passe'][0],
                                   request.form['ancien_mot_de_passe']) : 
                cur = con.cursor()
                nouveau_mdp = generate_password_hash(
                                                request.form['mot_de_passe_1'])
                requete_SQL = f"""UPDATE authentification 
                                  SET mot_de_passe = '{nouveau_mdp}'
                                  WHERE mail = '{mail}'"""
                
                cur.execute(requete_SQL)
                cur.close()
                return redirect('/changer_mdp_success')    
    return redirect('/changer_mdp_fail')

    
@app.route('/formulaire_authentification')
def authentification():
    if 'nom_de_compte' in session :
        return redirect("/")
    "Le formulaire de connexion se contente de récupéré l'adresse et le mdp"
    return render_template("formulaire_authentification.html")


@app.route('/traitement_authentification', methods=['POST'])
def traitement_authentification():
    """ traitement des informations d'authentification et connexion de 
    l'utilisateur. Les mêmes vérifications que pour un changement de mot de 
    passe sont réalisées. Si le formulaire passe ces vérifications alors
    l'utilisateur est rajouté dans la "session" flask comme utilisateur
    et une page est renvoyée avec un cookie qui permet d'identifier
    l'utilisateur. Ces deux chose sont équivalente il sera possible donc de
    traquer l'individu soit sur les valeurs contenu dans "sa" session ou 
    dans le cookie
       
    Une session est un cookie flask que l'utilisateur porte dans toutes ses
    étapes, le cookie est l'équivalent manuel. """
      
    mail = request.form['mail']
    con=lite.connect(ADRESSE_BDD_AUTH)
    with con:
        #Nou
        df=pd.read_sql_query(f'''SELECT DISTINCT mail FROM authentification
                                 WHERE mail = "{mail}"''', con)
                                 
        #Verifie si le compte existe deja                       
        if not df.empty:
            df=pd.read_sql_query(f'''SELECT DISTINCT mot_de_passe 
                                     FROM authentification
                                     WHERE mail = "{mail}"''', con)
            #Verifie si le mot de passe est le bon                   
            if check_password_hash(df['mot_de_passe'][0],
                                   request.form['mot_de_passe']) :
                
                df=pd.read_sql_query(f'''SELECT DISTINCT nom_de_compte 
                                         FROM authentification
                                         WHERE mail = "{mail}" ''', con)
                                         
                #Rajoute l'utilisateur a la session, récupère son nom 
                df=pd.read_sql_query(f'''SELECT DISTINCT nom_de_compte 
                                     FROM authentification
                                     WHERE mail = "{mail}"''', con)                                        
                session['nom_de_compte'] = df['nom_de_compte'][0]
                
                #Recupere le niveau de sécurité de l'utilisateur
                df=pd.read_sql_query(f'''SELECT DISTINCT niveau_de_compte 
                                     FROM authentification
                                     WHERE mail = "{mail}"''', con)
                #Session est un objet JSON, il n'accepte que les str et pas les
                #int donc on cast notre niveau de compte en str
                session['niveau_de_compte'] = str(df['niveau_de_compte'][0])
                
                return redirect("/connexion_success")
                #Creer le cookie qui au fond ne sert a rien dans l'instant
                #Pour transporter un cookie il est nécésaire de modifier la 
                #reponse du serveur à la requête HTML, au lieu de simplement
                #render_template/redirect on cree alors une reponse et on y 
                #Rajoute un cookie
                # reponse = make_response(redirect('/connexion_success'))                       
                # reponse.set_cookie('pseudo', df['nom_de_compte'][0])
                # return reponse
               
    return redirect("/connexion_fail")

@app.route('/deconnexion')
def deconnexion():
    """Le formulaire de deconnexion doit être fournis que si la personne est
       connectée, si elle ne l'est pas elle ne doit clairement pas être la """
    if 'nom_de_compte' in session : 
        return render_template("formulaire_deconnexion.html")
    return render_template("message_deconnexion_fail.html")

@app.route('/traitement_deconnexion')
def traitement_deconnexion():
    "Le formulaire de connexion se contente de récupéré l'adresse et le mdp"
    if 'nom_de_compte' in session :
        session.pop('nom_de_compte', None)
        session.pop('niveau_de_compte', None)
    return redirect("/")

@app.route('/test_de_la_session')
def test_de_la_session():
    if 'nom_de_compte' in session :
        print("je suis correctement connecté !")
        print(f"""Je suis l'utilisateur {session['nom_de_compte']}
                  J'ai pour valeur de compte {session['niveau_de_compte']}""")
    return redirect("/")

### Pages de gestion des erreurs
# Ici nous allons placer les pages qui correspondent aux erreurs de formulaire
#L'idée est à terme de rediriger vers de meilleurs endroits (comme la page
# profil) post connexion ou renvoyer sur le formulaire avec les cases remplies
# là ou ça ne posait pas de problèmes.
@app.route('/erreur_mdp')
def erreur_mdp() :
    return render_template('message_erreur_mdp.html')

@app.route('/creation_success')
def creation_success() :
    return render_template('message_creation_success.html')

@app.route('/creation_fail')
def creation_fail() :
    return render_template('message_creation_fail.html')

@app.route('/changer_mdp_success')
def changer_mdp_success() :
    return render_template('message_changer_mdp_success.html')

@app.route('/changer_mdp_success_fail')
def changer_mdp_success_fail() :
    return render_template('message_changer_mdp_success_fail.html')

@app.route('/connexion_success')
def connexion_success() :
    return render_template('message_connexion_success.html')

@app.route('/connexion_fail')
def connexion_fail() :
    return render_template('message_connexion_fail.html')

@app.errorhandler(404)
def page_not_found(error):
    title = "Un bug ?!"
    return render_template('404.html', title=title), 404

@app.errorhandler(500)
def internal_error(error):
    title = "Un bug ?!"
    return render_template('500.html' , title=title), 500

@app.errorhandler(405)
def method_error(error):
    return redirect('/')

### Lancement de l'application 
if __name__ == '__main__':
    app.run (debug = True, use_reloader=True)

    

    