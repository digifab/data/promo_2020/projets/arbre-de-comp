#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 29 14:11:33 2020

@author: Joseph Don Simoni
"""
### bibliotheque externe
import pandas as pd
import random
import string
import os


from peewee import Model, chunked, JOIN
from peewee import (TextField, IntegerField, DateField, ForeignKeyField)
from playhouse.sqlite_ext import (SqliteExtDatabase, AutoIncrementField)

from werkzeug.security import generate_password_hash
from numpy.random import randint
from datetime import datetime, date, timedelta

from functools import wraps

### declaration des constantes
FICHIER = "initialisation_base_peewee.xlsx"
NOM_DE_BASE = 'orm_minimaliste.db'
FICHIER_DE_SAUVEGARDE = 'orm_minimaliste_sauvegarde.xlsx'
LISTE_DATE =  [date(2020,4,1),
               date(2020,6,1),
               date(2020,9,24),
               date(2020,12,24)]

# Beaucoup_de_dates = [date(2020,1,1)]
# iterateur = 1
# while iterateur <= 365 :
#     Beaucoup_de_dates += [Beaucoup_de_dates[-1]+timedelta(days=1)]
#     iterateur += 1
# LISTE_DATE = Beaucoup_de_dates

### declaration des variables globales
"""Pour permettre la prise en charge des foreign_keys par peewee il est
nécésaire de déclarer ces pargmas a l'initialisation de la bdd"""
db = SqliteExtDatabase(NOM_DE_BASE, pragmas={'foreign_keys' : 1})

def timer(origin_func):
    import time
    
    @wraps(origin_func)
    def wrapper (*args, **kwargs):
        t1 = time.time()
        result = origin_func(*args, **kwargs)
        t2 = time.time() - t1
        print (f"{origin_func.__name__} a été exécutée en {t2}")
        
        return result
    return wrapper      

### declaration des Model peewee
class BaseModel(Model) :
    """Chaque models devra être capable de sauvegarder sa classe/table et de 
    la charger depuis un excel"""
    
    @classmethod
    def chargement_depuis_xls(cls, fichier) :
        xl = pd.read_excel(fichier, cls.__name__)
        dico = xl.to_dict('records')
        with db.atomic():
            cls.insert_many(dico).execute()
     
    @classmethod
    def sauvegarde_vers_xls(cls, fichier) :
        df = pd.DataFrame(list(cls.select().dicts()))
        with pd.ExcelWriter(fichier,mode='a', engine='openpyxl') as writer:   
            df.to_excel(writer, sheet_name=cls.__name__ ,index=False)
            writer.save()
            writer.close()
         
    class Meta :
        database = db

class Statut(BaseModel):
    """Statut d'une personne, l'id a pour but de servir de int de comparaison
    pour droit d'accès du site donc n'est pas autoincrement mais sert quand
    même de primary key"""
    ID_statut = IntegerField(primary_key = True, column_name="id")
    nom_de_statut = TextField()
    
class Personne(BaseModel) :
    """Une personne, possède toutes les caractéristique réquise et une foreign
    key vers statut qui n'est pas en lazy loading."""
    ID_personne = AutoIncrementField(column_name="id")
    nom = TextField()
    prenom = TextField()
    statut = ForeignKeyField(Statut)
    date_de_naissance = DateField()
    numero_de_tel = TextField(null = True)
    mail = TextField(null = True)
    description = TextField() 
    
class Connaissance(BaseModel) :
    """Une unité de connaissance, sert de structure à la logique arbre"""
    ID_connaissance = AutoIncrementField(column_name="id")
    pere = TextField(null = True)
    nom = TextField()
    rang = IntegerField()
    
class Evaluation(BaseModel) :
    """Une evaluation, pour limité les redondance d'info et facilité les join
    possède 3 foreign keys 2 correspondant au noteur/noté et 1 vers la 
    connaissance qu'elle évalue"""
    ID_note = AutoIncrementField(column_name="id")
    note = IntegerField()
    date_eval = DateField()
    
    ID_eleve = ForeignKeyField(Personne, backref='eleve',lazy_load=False)
    ID_noteur = ForeignKeyField(Personne, backref='formateur',lazy_load=False)
    ID_connaissance = ForeignKeyField(Connaissance,backref='connaissance',
                                      lazy_load=False)
    
    @staticmethod
    def evaluation_fictive(query_eleve, query_connaissance, query_formateur) :
        """une méthode de la Classe évaluation qui permet de récupéré une liste
        de dictionnaire correspondant au format demandé par peewee pour des 
        insert de masse, l'information contenue et des notes randomisé suivant
        le modèle de BDD.py des groupes précédent, 4 notes par formateur, pour
        chaque connaissance pour chaque élève
        """
        liste_query = [] 
        for eleve in query_eleve:
            for connaissance in query_connaissance : 
                for formateur in query_formateur :
                    for debut_periode in LISTE_DATE:
                        liste_query.append({
                            "note" : randint(0,11),
                            "date_eval" : (debut_periode +
                                           timedelta(randint(0,30))),
                            "ID_eleve" : eleve.ID_personne,
                            "ID_noteur" : formateur.ID_personne,
                            "ID_connaissance" : connaissance.ID_connaissance})
        return liste_query

class Authentification(BaseModel):
    """le site nécésite des authentification, celle-ci corresponde à un niveau
    de compte, un nom de compte, un mail et un mot de passe.
    le niveau de compte à pour but d'être lié au statut d'une personne."""
    ID_auth = AutoIncrementField(column_name="id")
    niveau_de_compte = ForeignKeyField(Statut)
    nom_de_compte = TextField()
    mail = TextField()
    mot_de_passe = TextField()

def randomString(l):
    """simple fonction de création d'un string random de taille l"""
    letters = string.ascii_letters
    return ''.join(random.choice(letters) for i in range(l))

class BDD_peewee() :
    """"Class correspondant à un gestionnaire de la BDD_peewee, à vertue a se 
    retrouver dans l'arbre de la promo, s'occupe a initialisé, sauvegarder,
    charger la BDD"""
    
    @staticmethod
    def initialisation(fichier, mode = "", admin = False) :
        """initialise une BDD neuve depuis un fichier xls, peut être une 
        sauvegarde, en mode fictif génère des milliers de notes pour
        Evaluations, peut aussi crée un compte Admin sur le site, géré mieux
        la sécruité à ce niveau la"""
        if os.path.isfile(NOM_DE_BASE):
          os.remove(NOM_DE_BASE)
        #créé une nouvelle BDD et initialise les modèles
        db.connect()
        db.create_tables([Personne, Connaissance,
                      Evaluation, Authentification, Statut])  
        #Charge depuis le fichier les info originale 
        Statut.chargement_depuis_xls(fichier)
        Personne.chargement_depuis_xls(fichier)
        Connaissance.chargement_depuis_xls(fichier)
        if mode == "fictif" : 
            #Crée les objet querry instancié en mémoire pour limité les
            #intéraction avec la bdd
            query_eleve = Personne.select().where(Personne.statut == "2")
            query_connaissance = Connaissance.select()
            query_formateur = Personne.select().where(Personne.statut == "1")
            #récupère toutes les info et le dico de querry, le met dans un 
            #gestionaire de transaction et insert le tout 100 par 100
            with db.atomic():
                for batch in chunked(Evaluation.evaluation_fictive(
                                                           query_eleve,
                                                           query_connaissance,
                                                           query_formateur),
                                                                        100) :
                    with db.atomic():
                        Evaluation.insert_many(batch).execute() 
        if admin :
            #crée le compte admin, a refaire probablement, mieux
            #devrais probablement avoir un system que tant que le pw/mail
            #n'a pas été changer le site ne tourne pas
            pw = randomString(15)
            fichier_txt = open("ADMIN_AUTH.txt","a+") 
            fichier_txt.writelines([f"Nom de compte = ADMIN",
                                    f"mail = digifab-ADMIN@digifab.fr",
                                    f"mot de passe = {pw}"])
            Authentification.create(niveau_de_compte = 0,
                                    nom_de_compte = "ADMIN",
                                    mail = "digifab-ADMIN@digifab.fr",
                                    mot_de_passe = generate_password_hash(pw))
            fichier_txt.close()
        db.close()
        
    @staticmethod
    @timer
    def test_Initialisation_BDD_peewee() :
        """Initialisation en fictif, avec timer de l'action"""
        BDD_peewee.initialisation(FICHIER, mode = "fictif")
    
    @staticmethod
    def sauvegarde_BDD_peewee(fichier, mode = "") :
        """Sauvegarde la BDD, toutes les feuille doivent être rentrer en code
        dur pour l'instant il faudrais regarder de coté de peewee si un BDD 
        peut itéré sur les Models dynamiquement"""
        if mode == "point_de_sauvegarde" :
            fichier = datetime.now().strftime("%m-%d-%Y--%H-%M-%S")+(
                                                                '---'+fichier)
        elif os.path.isfile(fichier) :
            os.remove(fichier) 
        writer = pd.ExcelWriter(fichier, engine='xlsxwriter')
        writer.save()
        Statut.sauvegarde_vers_xls(fichier)
        Connaissance.sauvegarde_vers_xls(fichier)
        Personne.sauvegarde_vers_xls(fichier)
        Evaluation.sauvegarde_vers_xls(fichier)
        Authentification.sauvegarde_vers_xls(fichier)

### Liste des Queries 

# @timer
# def query_personne_peewee() :
#     return Personne.select().where(Personne.statut == 2)

#Da big one
# @timer
def eval_vers_dico_where(cible) :
    """Cette query sert a résoudre la table évaluation, on y join deux fois
    personnes pour obtenir le nom et prenom de l'élève et du noteur ainsi que 
    la table connaissance pour accèder au nom"""
    élève = Personne.alias()
    noteur = Personne.alias()
    q = (Evaluation
                .select(Evaluation.ID_note,
                        Evaluation.note,
                        Evaluation.date_eval,
                            élève.nom.alias("nom_eleve"),
                            élève.prenom.alias("prenom_eleve"),
                                noteur.nom.alias("nom_noteur"),
                                noteur.prenom.alias("prenom_noteur"),
                                    Connaissance.nom.alias("nom_connaissance"))
                .join(élève, JOIN.LEFT_OUTER,
                      on=(Evaluation.ID_eleve == élève.ID_personne))
                .join(noteur, JOIN.LEFT_OUTER,
                      on=(Evaluation.ID_noteur == noteur.ID_personne))
                .join(Connaissance, JOIN.LEFT_OUTER,
                  on =(Evaluation.ID_connaissance ==
                                                 Connaissance.ID_connaissance))
                .where(Evaluation.ID_eleve == cible)
                .order_by(Evaluation.ID_connaissance)
                .dicts()) #Formatte tout ça dans un dico
    return q
    return q

### Deployment Main
if __name__ == "__main__":
    BDD_peewee.test_Initialisation_BDD_peewee()


# @timer
# def query_personne_SQLite3(con) :
#     q = '''SELECT "t1"."id",
#         "t1"."nom", "t1"."prenom",
#           "t1"."statut_id", "t1"."date_de_naissance",
#               "t1"."numero_de_tel", "t1"."mail", "t1"."description" 
#               FROM "personne" AS "t1" 
#               WHERE ("t1"."statut_id" = 2)'''
#     return pd.read_sql_query(q,con)

# @timer
# def eval_vers_dico_where_SQLite3 (cible, con) :
#     q = f'''SELECT "t1"."id",
#            "t1"."note",
#            "t1"."date_eval",
#                "t2"."nom" AS "nom_eleve",
#                "t2"."prenom" AS "prenom_eleve",
#                    "t3"."nom" AS "nom_noteur",
#                    "t3"."prenom" AS "prenom_noteur",
#                        "t4"."nom" AS "nom_connaissance" 
#             FROM "evaluation" AS "t1" LEFT OUTER JOIN "personne" AS "t2" 
#                                         ON ("t1"."ID_eleve_id" = "t2"."id") 
#                                       LEFT OUTER JOIN "personne" AS "t3" 
#                                         ON ("t1"."ID_noteur_id" = "t3"."id") 
#                                       LEFT OUTER JOIN "connaissance" AS "t4" 
#                                       ON ("t1"."ID_connaissance_id" = "t4"."id") 
#                                           WHERE ("t1"."ID_eleve_id" = 17)
#                                           ORDER BY "t1"."ID_connaissance_id"'''
#     return pd.read_sql_query(q,con)
    
# @timer
# def function_test_peewee():
#     dico = {}
#     for personne in query_personne_peewee():
#         dico[personne] = eval_vers_dico_where_peewee(personne)
#     return dico
 

# @timer
# def function_test_sql():       
#     con = lite.connect(NOM_DE_BASE) 
#     dico = {}    
#     for personne in query_personne_SQLite3(con).id :
#         dico[personne] = eval_vers_dico_where_SQLite3(personne, con)
#     return dico
        
# dico_peewee = function_test_peewee()
# dico_sql    = function_test_sql()

# print(len(dico_peewee))
# print(len(dico_sql))


#Format [{("date", (Noteur_nom, Noteur_prenom)) : note}]
    
# BDD_peewee.test_Initialisation_BDD_peewee()
    
# liste = []
# for x in eval_vers_dico_where_peewee(18) :
#     if x["nom_connaissance"] == "python" :
#         liste.append({(x["date_eval"], (x["nom_noteur"], x["prenom_noteur"])) : x["note"]})
        
        
# print(liste)

# #date_eval vers string
# for e in eval_vers_dico_where_peewee(18) :
#     print(e["date_eval"])
#     print(type(e["date_eval"]))
#     print(e["date_eval"].strftime("%Y-%m-%d"))
#     print(type(e["date_eval"].strftime("%Y-%m-%d")))

### ARTEFACT

# def Excel_sheet_to_csv():    
#     xl = pd.ExcelFile(FICHIER)
#     for sheet in xl.sheet_names:
#     	df = xl.parse(sheet)
#     	df.to_csv(sheet+".csv", index=False)

# class BaseModel(Model) :
#     def chargement_depuis_xls(self, fichier) :
#         xl = pd.read_excel(FICHIER, self.__name__)
#         dico = xl.to_dict('records')
#         self.insert_many(dico).execute()    
    
#     def lecture_xls(fichier, ligne, sheet_name) :
#         xl = pd.read_excel(fichier,sheet_name)       
#         i = 0 
#         donnee_ligne = []
#         while i < len(xl.iloc[ligne]) :   
#             donnee_ligne.append(xl.iloc[ligne][i])
#             i+=1
#         return donnee_ligne

#     def ecriture_xls(fichier, sheet_name, donnee_ligne):
#         pass

#     class Meta :
#         database = db

# def peuple_BDD(fichier) :
#     def xls_vers_Personne(fichier):
#         def ajouter_une_Personne_xls(fichier, ligne):
#             liste = lecture_xls(fichier, ligne, "Personne")
#             Personne.create(nom = liste[0],
#                             prenom = liste[1],
#                             statut = liste[2],
#                             date_de_naissance = liste[3],
#                             numero_de_tel = liste[4],
#                             mail = liste[5],
#                             description = liste[6]) 
#         xl = pd.read_excel(fichier, "Personne")
#         i = 0
#         while i < len(xl) :
#             ajouter_une_Personne_xls(fichier, i)
#             i+=1

#     def xls_vers_Statut(fichier):
#         def ajouter_un_Statut_xls(fichier, ligne):
#             liste = lecture_xls(fichier, ligne, "Statut")
#             Statut.create(ID_statut = liste[0],
#                           nom_de_statut = liste[1]) 
#         xl = pd.read_excel(fichier, "Statut")
#         i = 0
#         while i < len(xl) :
#             ajouter_un_Statut_xls(fichier, i)
#             i+=1
            
#     def xls_vers_Connaissance(fichier):
#         def ajouter_une_Connaissance_xls(fichier, ligne):
#             liste = lecture_xls(fichier, ligne, "Connaissance")
#             Connaissance.create(pere = liste[0],
#                             nom = liste[1],
#                             rang = liste[2],) 
#         xl = pd.read_excel(fichier, "Connaissance")
#         i = 0
#         while i < len(xl) :
#             ajouter_une_Connaissance_xls(fichier, i)
#             i+=1 
        
#     xls_vers_Connaissance(FICHIER)        
#     xls_vers_Personne(FICHIER)        
#     xls_vers_Statut(FICHIER)
    
# def __test__create_eleve():
#     Personne.create(nom = "415465465",
#         prenom = "pierre",
#         statut = "e",
#         date_de_naissance ="1990/11/11",
#         numero_de_tel = "0",
#         mail = "1@k",
#         description ="a")
#     Personne.create(nom = "dupond",
#         prenom = "paul",
#         statut = "e",
#         date_de_naissance ="1990/11/11",
#         numero_de_tel = "0",
#         mail = "1@k",
#         description ="a")
                
                
# def Evaluation_fictive() : 
# #Iteration sur  d'élève 
#     for eleve in Personne.select().where(Personne.statut == "2"):
#         #iteration sur connaissance
#         for connaissance in Connaissance.select() : 
#             #iteration sur formateur
#             for formateur in Personne.select().where(Personne.statut == "1") :
#                 for debut_periode in [date(2020,4,1),
#                                       date(2020,6,1)]:
#                     Evaluation.create(  note = randint(0,11),
#                                         date_eval = (debut_periode +
#                                                       timedelta(randint(0,61)) ),  
#                                         ID_eleve = eleve.ID_personne,
#                                         ID_noteur = formateur.ID_personne,
#                                         ID_connaissance = connaissance.ID_connaissance)                