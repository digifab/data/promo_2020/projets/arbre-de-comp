# -*- coding: utf-8 -*-
"""
Created on Fri Apr 24 21:23:58 2020

@author: Joseph Don Simoni

MODELISATION ARBRE DE COMPETENCE

Ce module à pour but de créer les structures permettant de modélisé un
arbre qui est une représentation des connaisance d'un individu.

Cette arbre est composé de noeuds qui represente une connaisance individuel
ainsi que les évaluation de ces connaissances. 

Le choix d'un arbre répond à la nécésité de modélisé la hierarchie des 
compétence ou une compétence peux posséder des sous compétence et ainsi
de suite. 

Ce module dépend d'une infrastructure SQLite déployé par Peewee établi dans 
le module annexe tests_modules_bdd.py

"""
### Bibliothèque général

from datetime import datetime
import matplotlib.pyplot as plt
import numpy as np

from playhouse.sqlite_ext import SqliteExtDatabase

### Bibliothèque interne au projet

from tests_modules_bdd import (Statut, Connaissance, Personne,
                               Evaluation, Authentification, BDD_peewee,
                               eval_vers_dico_where)

### Declaration des Constantes

TEST = True
NOM_DE_BASE = 'orm_minimaliste.db'
FICHIER_DE_SAUVEGARDE = 'orm_minimaliste_sauvegarde.xlsx'

### Déclaration des variable globale
    
db = SqliteExtDatabase(NOM_DE_BASE, pragmas={'foreign_keys' : 1})
db.connect()

class noeud_connaissance() :
    """Une classe representant un noeud de l'arbre de connaissance 
       unique de chaque élève.
       Il contient l'équivalent d'une ligne de la table Connaisance ainsi
       qu'un dictionaire de toutes les evaluations d'un élève sous le format
       clef = date definition = tuple (noteur, note)"""
    def __init__(self, id, nom, rang, pere=None, evaluation = {}) :
        self.connaissance_id = id
        self.pere = pere
        # self.liens = {"pere" : [pere],
        #               "fils" : []}
        self.nom = nom 
        self.rang = rang
        
        #Format [{("date", (Noteur_nom, Noteur_prenom)) : note}]
        self.evaluation = {}
        self.moyenne = -1

        #Initialisé a -1, sera modifié par fonction de l'arbre 
        self.place_fraterie = -1
        self.taille_fraterie = -1
        self.facteur_fraterie = -1
        self.angle = -1
    
    def fils(self, liste_noeud) :
        """renvoie la liste [noeud ayant self pour père] dans la liste en param"""
        liste = []
        for n in liste_noeud :
            if n.pere == self.nom :
                liste.append(n)
        return liste
    
    def moyenne_date(self, date_obs, noteur = []) : 
        """Cette fonction doit permettre de sortir la moyenne des notes avant une date
        precise de tout les noteur ou de plusieurs noteurs ou d'un noteur passé en
        paramètre dans une liste"""
        date_obs = datetime.strptime(date_obs,"%Y/%m/%d").date()
        i = 0
        total_note = 0
        for evaluation in self.evaluation : #parcours
            if (evaluation[0] <= date_obs) : #isolation
                if evaluation[1] in noteur or noteur == [] :
                    total_note += self.evaluation[evaluation] #action
                    i += 1
        self.moyenne = total_note/i
        return total_note/i
        
    def renvoie_pere(self, liste_noeud) :
        """Renvoie le pere du noeud"""
        for n in liste_noeud : 
            if n.nom == self.pere :
                return n
    

class arbre_personnel():
    """Cette classe represente l'arbre de competence d'un seul individu
    elle possèd
    
    
    """
    def __init__(self, personne, connaissance, evaluation):        
        self.personne = personne #Pointeur vers une querry qui contien les info des personnes
        self.liste_noeud_connaissance = self.peuple_liste_noeud(connaissance,
                                                                evaluation)
        
    def peuple_liste_noeud(self, connaissance, evaluations) : 
        """Peewee peuple un arbre qui a pour structure Connaissance avec toutes
           les eval lié a l'id de la personne"""
           #(self, id, nom, rang, pere=None, evaluation = {})
        liste_noeud = []
        #Cree la structure de l'arbre
        for c in connaissance : 
            liste_noeud += [(noeud_connaissance(c.ID_connaissance, c.nom,
                                                  c.rang,pere=c.pere))]                
        # #Peuple les évaluation
        for e in evaluations :
            for n in liste_noeud : 
                if e["nom_connaissance"] == n.nom :
                #     #Format {("date", (Noteur_nom, Noteur_prenom)) : note}
                    n.evaluation[(e["date_eval"],
                                   (e["nom_noteur"],
                                    e["prenom_noteur"]))] = e["note"]
        return liste_noeud
    
    def sort_by_rang(self, generation):
        '''"renvoi la liste gen contenant les noeud de "generation" en
        paramètre de la liste_noeud''' 
        gen=[]
        for n in self.liste_noeud_connaissance : 
            if n.rang == generation :
                gen.append(n)
        return gen
        
    def fraterie(self, noeud) :
        """Renvoie la taille de la fraterie du noeud"""
        liste = []
        for n in self.liste_noeud_connaissance : 
            if noeud.pere == n.pere :
                liste+= [n]
        return liste
    
    def caracterise_noeuds(self) :
        """Fonction ayant pour but de prendre la liste de noeud de connaissance
        de l'arbre et caractérisé ces noeud par rapport aux autres noeuds, 
        definir la fraterie : la taille et la place de chaque noeud dans cette
        fraterie
        definir le facteur de la fraterie : la factorielle de la population
        des frateries des noeud pere
        le calcul de l'angle de projection pour l'arbre de competence
        """
        def definie_fraterie(fraterie_noeud) :
            """Palce fraterie : la nature grand frère-petit frère au sein de 
            la fraterie
               taille_fraterie : la longeur de la liste de frère"""
            i = 0
            for n in fraterie_noeud:
                condition_1 = (n.place_fraterie == -1)
                condition_2 = (n.taille_fraterie == -1)
                if condition_1 or condition_2:
                    n.place_fraterie = i   
                    n.taille_fraterie = len(self.fraterie(n))
                    i += 1
                
        def facteur_fraterie(noeud) :
            """Calcul le facteur de fraterie du noeud, ce facteur sert
            à determiner la position du noeud dans l'arbre complet'
            """
            individu = noeud
            pere = individu.renvoie_pere(self.liste_noeud_connaissance)
            facteur_fraterie = 1 
            while pere != None :
                facteur_fraterie *= individu.taille_fraterie
                individu = pere
                pere = individu.renvoie_pere(self.liste_noeud_connaissance)
            noeud.facteur_fraterie = facteur_fraterie
            noeud.facteur_fraterie *= individu.taille_fraterie
            
        def calcul_angle(liste_noeud, node):
            """ Calcul de l'angle de positionement du noeud. Ce calcul depends
            des elements suivant : le nombre de noeud total, la place de 
            chaque noeud dans sa fraterie, la factorielle de la valeur
            fraterie de chaque noeud. ce calcul ce fait du Point de Vue 
            de chaque noeud de l'arbre.'
            """
            individu = node
            pere = node.renvoie_pere(self.liste_noeud_connaissance)
            angle = np.pi/6
            while pere != None :
                id_ = individu.place_fraterie
                angle += 2*np.pi/individu.facteur_fraterie*(id_+0.5)
                angle -= np.pi/pere.facteur_fraterie
                individu = individu.renvoie_pere(self.liste_noeud_connaissance)
                pere = individu.renvoie_pere(self.liste_noeud_connaissance)
            id_ = individu.place_fraterie
            angle += 2*np.pi/individu.taille_fraterie*(id_-0.5)
            node.angle = angle
            pass
        
        for n in self.liste_noeud_connaissance :
            definie_fraterie(self.fraterie(n))
        for n in self.liste_noeud_connaissance :
            facteur_fraterie(n)
        for n in self.liste_noeud_connaissance : 
            calcul_angle(self.liste_noeud_connaissance, n)            
            
            
    def dessine_arbre_competences(self, date_obs, liste_noteur = [],
                                  nom_figure='img.png'): 
        """Dessine l'arbre avec matplotlib"""
        
        
        self.caracterise_noeuds()
        
        #calcul position
        pos= {}
        for n in self.liste_noeud_connaissance :
            rayon = n.rang
            angle = n.angle
            x = rayon*np.cos(angle)
            y = rayon*np.sin(angle)
            pos[n.nom] = (x,y)
            
        #Couleurs
        couleurs = [] 
        for n in self.liste_noeud_connaissance :
            note = n.moyenne_date(date_obs, noteur = liste_noteur) 
            couleur = note/10.0
            couleurs.append(couleur)

        #place les points    
        fig, ax = plt.subplots(figsize=(8, 8))    
        for n in self.liste_noeud_connaissance:
            ax.scatter(pos[n.nom][0],pos[n.nom][1], s=100,
                       c="blue", alpha = n.moyenne/10)
            ax.annotate(n.nom, pos[n.nom])
            
        #Print debug
        # for n in self.liste_noeud_connaissance:
        #     print(f"""Je suis le noeud : {n.nom}\n
        #           J'ai pour pere le noeud : {n.pere}\n
        #           j'ai pour rang le rang : {n.rang}\n
        #           j'ai pour place frat : {n.place_fraterie}\n
        #           j'ai pour taille frat : {n.taille_fraterie}\n
        #           j'ai pour fact frat : {n.facteur_fraterie}\n""")
            
        #place les lignes 
        for n in self.liste_noeud_connaissance:
            if n.pere != "nan" :            
                ax.plot([pos[n.nom][0],pos[n.pere][0]],
                        [pos[n.nom][1],pos[n.pere][1]], 'k-', alpha = 0.33)
                
            
        maxrang = 1  
        while self.sort_by_rang(maxrang) != []:
            T = np.linspace(0, 2*np.pi, 1000)
            x = maxrang*np.cos(T)
            y = maxrang*np.sin(T)
            #Dessine
            plt.plot(x, y, alpha=0.7)
            maxrang += 1
        pass
        
class arbre_de_la_promo():
    def __init__(self, connecteur_bdd) :
        bdd = connecteur_bdd
        bdd.bind([Connaissance, Statut, Personne, Evaluation, Authentification])
        self.arbres = self.peuple_arbre_promo()
        
    def peuple_arbre_promo(self) : 
        arbres = []
        q_Connaissance = Connaissance.select()
        q_Personne = Personne.select().where(Personne.statut == 2)
        for personne in q_Personne :
            arbres.append(arbre_personnel(personne,
                                       q_Connaissance,
                                   eval_vers_dico_where(personne.ID_personne)))
        return arbres
    
    def dessine_arbre_eleve(self, eleve_id, date_obs, liste_noteur):
        for arbre in self.arbres : 
            if arbre.personne.ID_personne == eleve_id :
                arbre.dessine_arbre_competences(date_obs,
                                                liste_noteur=liste_noteur)
    
    
    def promo_to_xl():
        pass
    def xl_to_promo():   
        pass 

### Deployment test
   
if __name__ == "__main__": 
    # BDD_peewee.test_Initialisation_BDD_peewee()
    db = SqliteExtDatabase(NOM_DE_BASE)    
    
    arbre_promo = arbre_de_la_promo(db)
    # # arbre_promo.arbres[17].caracterise_noeuds()
    arbre_promo.arbres[17].dessine_arbre_competences("2020/06/01")




"2020/06/01"
# print(type(arbre_promo.arbres[17].liste_noeud_connaissance))

# for n in arbre_promo.arbres[17].liste_noeud_connaissance :
#     print(n.nom)


#Excel > dataframepanda > sqlite3 > bdd > dico > nx.DiGraph > Arbre



#Excel > Objet x df > BDD
#              L___ > Arbre















